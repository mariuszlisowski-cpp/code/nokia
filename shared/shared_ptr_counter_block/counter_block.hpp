#pragma once

#include <atomic>

class counter_block {
public:
    counter_block() : counter_(0) {}
    ~counter_block() {}

    counter_block(counter_block&&) = delete;
    counter_block(const counter_block&) = delete;
    counter_block& operator=(const counter_block&) = delete;
    counter_block& operator=(counter_block&&) = delete;

    void increment() noexcept {
        counter_++;
    }
    void decrement() noexcept {
        counter_--;
    }
    unsigned get() const noexcept {
        return counter_.load();
    }
    void reset() {
        counter_ = 0;
    }

private:
    std::atomic<unsigned> counter_;
};

