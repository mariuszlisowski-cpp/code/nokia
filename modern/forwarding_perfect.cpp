#include <string>

class Person {
public:
    Person(std::string name, std::string surname)							// parametrized c'tor
        : m_name{std::move(name)}
        , m_surname{std::move(surname)}
    {}

private:
    std::string m_name, m_surname;
};

class StringBuilder {
public:
    StringBuilder(std::string& destination)
        : m_destination{std::move(destination)}
    {}

private:
    std::string m_destination;
};

/* perfect forwarding */
template <typename T, typename... Args>
T construct(Args&&... args) {                                               // 1st string&&, 2nd const string&
    return T{std::forward<Args>(args)...};                                  // 1st forwarded as string&&
}                                                                           // 2nd forwarded as const string&

int main() {
    auto name1 = std::string{"Jan"};
    auto name2 = std::string{"Jan"};
    auto name3 = std::string{"Jan"};
    const auto surname = std::string{"Kowalski"};

	auto person = construct<Person>(std::move(name3), surname);   	        // perfect forwarding is use

    auto str = std::string{};
    auto builder = construct<StringBuilder>(str);

    return 0;
}
