/* Copy elision refers to a compiler optimization technique that eliminates unnecessary copying of objects.
 */
#include <iostream>

int n = 0;

struct C {
    explicit C(int) {
        std::cout << "parametrized c'tor" << std::endl;
    }
    C(const C&) { ++n; }                                    // the copy c'tor has a visible side effect
    // C(const C&) = delete;                                // will work as not used in this example

    /* may be deleted as not used here */
    C(C&&) = delete;
    C& operator=(C& ) = delete;
    C& operator=(C&& ) = delete;
};                                                          // it modifies an object with static storage duration

int main() {
    C c1(42);                                               // direct initialization, calls C::C(int)
    C c2 = C(42);                                           // copy initialization, should call C::C(const C&)

    std::cout << n << std::endl;                            // 0 if the copy was elided (constructed directly)
                                                            // 1 otherwise as n was incremented (copying)
}
