/* Virtual Method Table: mechanism used to support dynamic dispatch (action of finding the right function to call)
   vtables exist at the class level, meaning there exists a single vtable per class,
   and is shared by all instances */

#include <iostream>

/* static dispatch */
class A {
  public:
    void foo() {                                                            // early binding
        std::cout << "this is foo" << std::endl;
    }
};

/* dynamic dispatch */
/* vpointer B -> vtable B
                  foo     ->  B::foo
                  bar     ->  B::bar  */
class B {                                                                   // class contains Virtual Table (vable)
                                                                            // two entries for each virtual method
                                                                            // pointing to B::foo & B::bar
public:
    virtual void foo() {
        std::cout << "This is B's implementation of foo" << std::endl;      // late binding...
    }                                                                       // finding the right function
                                                                            // definition at runtime
    virtual void bar() {
        std::cout << "This is B's implementation of bar" << std::endl;      // saa
    }
private:
    // vpointer (created by a compiler)                                     // pointing to Vtable B
};

/* vpointer C -> vtable C
                  foo     ->  B::foo                                        // NOT overwritten
                  bar     ->  C::bar */
class C : public B {                                                        // class contains Virtual Table (vable)
                                                                            // two entries for each virtual method
                                                                            // pointing to C::bar (overwritten) and
                                                                            // B:foo which is NOT overwritten
public:
    void bar() override {
        std::cout << "This is C's implementation of bar" << std::endl;      // virtual method overwritten
    }
private:
    // vpointer (created by a compiler)                                     // pointing to Vtable C
};

int main() {
    B* b = new C();                                                         // upcasting (implicit)
    b->bar();                                                               // C class method executed
                                                                            // C object vpointer used...
                                                                            // to point to vtable C
    return 0;
}
