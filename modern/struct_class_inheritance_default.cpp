class BaseClass {
public:
    int x;
};

class DerivedPrivately : BaseClass {};                              // is equivalent to : private BaseClass

struct DerivedPublicly : BaseClass {};                              // is equivalent to : public BaseClass

int main() {
    DerivedPrivately d_private;

    /* ERROR: inheritance is private
    d_private.x = 7; */

    DerivedPublicly d_public;
    d_public.x = 7;

    return 0;
}   
