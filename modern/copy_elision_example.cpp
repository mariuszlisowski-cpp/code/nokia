/* Copy elision is an optimization implemented by most compilers to prevent extra copies (potentially expensive).
   Copy elision can be applied even if copying/moving the object has side-effects.
*/
#include <iostream>

struct C {
    C() {}
    C(const C&) { std::cout << "A copy was made.\n"; }              // side effect is printing this message
};

C f() {
    return C();                                                     // copy elision may take place
}

int main() {
    C obj = f();                                                    // copy elision may take place

    return 0;
}
