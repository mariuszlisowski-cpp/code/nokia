#include <iostream>

struct A {
    virtual void f() {
        std::cout << "A" << std::endl;
    }
};

struct B : A {
    void f() override {
        std::cout << "B" << std::endl;
    }
};

void val(A a) { a.f(); }
void lref(A& a) { a.f(); }
void rref(A&& a) { a.f(); }                             // rvalue reference

int main() {
    B b;

    val(b);
    lref(b);
    rref(static_cast<A&&>(b));
    rref(std::move(b));                                 // b object should NOT be used   

    return 0;
}
