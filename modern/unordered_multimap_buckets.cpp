#include<iostream>
#include<string>
#include<unordered_map>

int main() {
	std :: unordered_multimap <int, std :: string> um{
		{1, "I   "},
		{1, "I   "},
		{1, "I   "},
		{1, "I   "},
		{2, "love"},
		{7, "me  "},
		{8, "you "},
		{5, "her "},
		{3, "him "},
		{6, "it  "},
		{4, "us  "},
		{9, "them"}
	};

    for(auto& pair : um) {
		std::cout << "Element[" << pair.first << ":" << pair.second << "]"
		          << " is in Bucket #" << um.bucket(pair.first)
                  << ", bucket size: " << um.bucket_size(pair.first) << std::endl;
	}

	return 0;	
}
