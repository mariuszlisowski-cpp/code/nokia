#include <iostream>

struct A {
    virtual void f() {
        std::cout << "A" << std::endl;
    }
};

struct B : A{
    void f() override {
        std::cout << "B" << std::endl;
    }
};

void g(A& a) { a.f(); }                             // expects lvalue ref
void h(A a) { a.f(); }                              // value (no polymophic behaviour)
void i(A&& a) { a.f(); }                            // expects rvalue ref

template<typename T>
void j(T&& t) { t.f(); }                            // forwarding reference (universal)

int main() {
    B b;
    g(b);                                           // B (passed by ref)
    h(b);                                           // A (passed by value)
    // g(B());                                      // ERROR: expects lvalue only
    h(B());                                         // A (passed by value)
    std::cout << "-" << std::endl;

    A a = b;
    j(a);                                           // A, passed as A& (forwarding reference)
    j(b);                                           // B, passed as B& (forwarding reference)

    i(std::move(b));                                // B
    // i(b);                                        // ERROR: expect rvalue only

    return 0;
}
