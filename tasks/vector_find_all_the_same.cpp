#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector<int> v = {3, 7, 3, 11, 3, 3, 2};
    sort(v.begin(), v.end());

    using IteratorPair =
        std::pair<std::vector<int>::iterator, std::vector<int>::iterator>;

    
    IteratorPair range = equal_range(v.begin(), v.end(), 3);                    // alternatively 'auto' keyword
    std::copy(range.first, range.second,
                  std::ostream_iterator<int>(std::cout, " "));

    return 0;
}
