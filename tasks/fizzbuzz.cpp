#include <algorithm>
#include <iostream>

void fizzbuzz(int max) {
    for (int i = 1; i < max; ++i) {
        if (i % 5 == 0 && i % 3 == 0) {
            std::cout << i << " fizzbuzz" << std::endl;
        } else if (i % 5 == 0) {
            std::cout << i << " buzz" << std::endl;
        } else if (i % 3 == 0) {
            std::cout << i << " fizz" << std::endl;
        }
    } 
}

int main() {
    fizzbuzz(18);

    return 0;
}
