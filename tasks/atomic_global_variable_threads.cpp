#include <atomic>
#include <iostream>
#include <mutex>
#include <thread>

std::atomic<int> i{};

void thread1() {
    while (1) {
        i = i + 1;
        if (i == 5000) {
            break;
        }
    }
}

void thread2() {
    while (1) {
        i = i + 1;
        if (i == 5000) {
            break;
        }
    }
}

int main() {
    std::thread t1(thread1);
    std::thread t2(thread2);

    t1.join();
    t2.join();

    std::cout << i << std::endl;

    return 0;
}
