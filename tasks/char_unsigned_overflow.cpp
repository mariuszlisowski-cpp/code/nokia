#include <iostream>

int main() {
    char c_min = -128;                              // [-128, 127] -> [-128, -1] + [0, 127]
    char c_max = 127;                               // [-128, -1] = 128 elements
                                                    // [0, 127]   = 128 elements
                                                    // total 256

    // char c = 127 + 1;                            // -128 as overflow
    // char c = 127 + 2;                            // -127 as overflow
    // char c = 127 + 3;                            // -126 as overflow
    
    // char c = 253;                                // -3 as overflow
    // char c = 254;                                // -2 as overflow
    // char c = 255;                                // -1 as overflow
    // char c = 256;                                // 0 as overflow
    // char c = 257;                                // 1 as overflow
    // char c = 258;                                // 2 as overflow
    // char c = 259;                                // 3 as overflow
    // std::cout << (int)c << std::endl;
    
    unsigned char uc_min = 0;                       // [0, 255]
    unsigned char uc_max = 255;                     // saa

    // unsigned char uc = 255 + 1;                  // 0 as overflow
    // unsigned char uc = 255 + 2;                  // 1 as overflow
    // unsigned char uc = 255 + 3;                  // 2 as overflow
    // std::cout << (int)uc << std::endl;

    return 0;
}
