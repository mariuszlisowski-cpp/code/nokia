#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>

auto reverseCPP_a(const std::string& str) {
    std::string reversed;
    std::copy(str.crbegin(), str.crend(), std::back_inserter(reversed));

    return reversed;
}

auto reverseCPP_b(const std::string& str) {
    std::string reversed{str};
    std::reverse(reversed.begin(), reversed.end());

    return reversed;
}

auto reverseC(const std::string& str) {
    std::string reversed;
    for (int i = str.size() - 1; i >= 0; --i ) {
        reversed += str[i];
    }
    return reversed;
}

int main() {
    std::string str{"abcd"};

    std::cout << reverseCPP_a(str) << std::endl;
    std::cout << reverseCPP_b(str) << std::endl;
    std::cout << reverseC(str) << std::endl;

    return 0;
}
