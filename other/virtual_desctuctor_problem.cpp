#include <iostream>

struct A {
  A() { std::cout << "A()" << std::endl; }
  ~A() { std::cout << "~A()" << std::endl; }                            // destructor is NOT virtual...

  virtual void foo() {}
};

class B : public A {
public:
  B() { std::cout << "B()" << std::endl; }
  ~B() { std::cout << "~B()" << std::endl; }                            // thus this d'tor is NOT called
};

int main() {
  A *ptr = new B;
  delete ptr;                                                           // no B' destructor called

  return 0;
}
