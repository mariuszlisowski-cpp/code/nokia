#include <iostream>

class A {
public:
    void func() {
        std::cout << "func() was called" << std::endl;
    }
};

class B : public A {};

class C : public A {};

class D : public B, public C {};                                    // multi-inheritance

/*    A
    /   \
   B     C
    \   /
      D                                                             // multi-ingerited from B & C
 */

int main() {
    D d;
    /* ambiguous call here
       should I use A inherited by B or A inherited by C?

    d.func();  */

    d.B::func();                                                    // overcome ambiguity
    d.C::func();                                                    // saa

    return 0;
}
