#include <iostream>
#include <memory>

struct Base {
    Base() {
        std::cout << "Base c'tor" << std::endl;
    }
    virtual ~Base() {}

    virtual void meow(int vol = 5) const {
        std::cout << "Base: meow: volume " << vol + 5 << std::endl;
    }
    void purr(int vol = 5) const {
        std::cout << "Base: purr: volume " << vol + 5 << std::endl;
    }
};

struct Derived : Base {
    Derived() {
        std::cout << "Derived c'tro" << std::endl;
    }
    virtual ~Derived() {}

    void meow(int vol = 10) const override {
        std::cout << "deriv: meow: volume " << vol + 10 << std::endl;
    }
    void purr(int vol = 10) const {
        std::cout << "deriv: purr: volume " << vol + 10 << std::endl;
    }
};

int main() {
    std::unique_ptr<Base> object(new Derived);

    object->meow();
    object->purr();

    Derived second;
    second.meow();

    return 0;
}
