#include <iostream>
#include <vector>

void func(int& x, const int& y) {
    x = 1;
    std::cout << y << std::endl;
}

int main() {
    std::vector<int> v{ 2, 3, 4 };

    for(auto e : v){
        e *= e + v.front();                 // work on copy
    }

    for (auto e : v) {
        std::cout << e << ' ';              // thus no change
    }

    return 0;
}
