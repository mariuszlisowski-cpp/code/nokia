#include <iostream>

class Person {
    virtual void foo() {}                                                   // be polimorphic
};                                                                          // for use with dynamic_cast

class Employee : public Person {
public:
    Employee(std::string first_name, std::string last_name, double sal) {
        first_name_ = first_name;
        last_name_ = last_name;
        salary_ = sal;
    }
    void show() {
        std::cout << last_name_ << ", " << first_name_
                  << ", salary: " << salary_ << std::endl;
    }
    void addBonus(double bonus) { salary_ += bonus; }

    std::string first_name_;
    std::string last_name_;
    double salary_;
};

class Manager : public Employee {
public:
    Manager(std::string first_name, std::string last_name,
            double sal, double commmision)
        : Employee(first_name, last_name, sal)
    {
        commision_ = commmision;
    }
    double getCommision() { return commision_; }

    double commision_;
};

class Clerk : public Employee {
public:
    Clerk(std::string first_name, std::string last_name,
          double sal, Manager* man)
        : Employee(first_name, last_name, sal)
    {
        manager = man;
    }
    Manager* getManager() { return manager; }

    Manager* manager;
};

void reward(Employee* emp) {
    emp->addBonus(200);                                                     // methods of Employee class
    emp->show();                                                            // will work with every
}                                                                           // derived object

int main() {
    /* upcasting - usually SAFE */
    Employee* emp;                                                          // pointer to base class object

    Manager m1("Steve", "Kent", 2000, 0.2);                                 // object of derived class
    Clerk c1("Kevin", "Jones", 1000, &m1);                                  // SAFE: clerk is always an empoloee

    emp = &m1;                                                              // implicit upcasting
    std::cout << emp->first_name_ << std::endl;                             // emp points to an object of Manager
    std::cout << emp->salary_ << std::endl;                                 // see line below to compare

    /* fails because upcasting is used
    std::cout << emp->getCommision(); */

    /*  advantage & use of upcasting */
    reward(&c1);                                                            // reward clerk
    reward(&m1);                                                            // reward manager
    std::cout << "Manager of " << c1.first_name_ << " is "
         << c1.getManager()->first_name_ << std::endl;

    /* downcasting - usually UNSAFE */
    // safe dowcasting (emplyee who is a manager -> manager)
    Manager* m2 = static_cast<Manager*>(emp);                               // UNSAFE: employee is NOT always
                                                                            // manager but it works here as
    std::cout << m2->getCommision() << std::endl;                           // emp points to an object of Manager

    // unsafe downcasting (employee -> manager)
    Employee e1("Peter", "Green", 1400);                                    // regular employee
    Manager* m3 = static_cast<Manager*>(&e1);                               // UNSAFE: no commision specified
    // std::cout << m3->getCommision() << std::endl;                        // ERROR: UB (undefined behaviour)

    // safe downcasting (as returns nullptr if not possible)
    Manager* m4 = dynamic_cast<Manager*>(&e1);                               // UNSAFE: no commision specified
    if (m4) {
        std::cout << m4->getCommision() << std::endl;
    } else {
        std::cout << "Cannot safely cast to Manager!" << std::endl;
    }

}
