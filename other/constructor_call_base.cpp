#include <iostream>

struct Base {
    Base() {
        std::cout << "Elephant" << std::endl;
    }
};

struct Mid1 : virtual public Base {};           // base c'tor call
struct Mid2 : virtual public Base {};
struct Mid3 : virtual public Base {};
struct Mid4 : public Base {};                   // base c'tor call
struct Mid5 : public Base {};                   // base c'tor call

struct Derived : public Mid1,
                 public Mid2,                   // NO base c'tor call
                 public Mid3,                   // NO base c'tor call
                 public Mid4,
                 public Mid5
{};

int main() {
    Derived d;

    return 0;
}
