#include <iostream>

class A {
public:
    std::string name;
};

class B : public A {};

class C {};

int main() {
    C c;
    B b;
    b.name = "foo";

    A* aptr = &b;                                               // B -> A upcasting (implicit) SAFE
    B* bptr = static_cast<B*>(aptr);                            // A -> B downcasting (explicit) UNSAFE
    std::cout << bptr->name << std::endl;
    
    // C* cptr = static_cast<C*>(aptr);                         // <- this will not work, as C is not A's descendant
    
    return 0;
}
