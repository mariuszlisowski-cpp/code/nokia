#include <exception>
#include <iostream>
#include <stdexcept>

int foo() {
    throw std::runtime_error("Ups...");
}

struct A {
    A() try : value(foo()) {                                                // initialize list
        // creating an object
    } catch (const std::exception& e) {
        std::cout << "c'tor exception" << std::endl;
        throw std::logic_error("thrown from c'tor");
    }

    int value;
};

int main() {
    try {
        A a;
    } catch (const std::logic_error& le) {
        std::cout << "caught from c'tor: " << le.what() << std::endl;
    }

    return 0;
}
