#include <iostream>

struct Interface {
    virtual void say() = 0;
};

class First : public Interface {
public:
    void say() override {
        std::cout << "> say from first" << std::endl;
    }
};

class Second : public Interface {
public:
    void say() override {
        std::cout << "> say from second" << std::endl;
    }
};

void say(Interface& iface) {
    iface.say();
}

int main() {
    First first;
    Second second;

    /* pointer */
    Interface* i_ptr = &first;
    i_ptr->say();                                                       // from first

    i_ptr = &second;
    i_ptr->say();                                                       // from second

    /* reference */
    Interface& i_ref = first;
    i_ref.say();
    
    i_ref = second;                                                     // CANNOT reassing a referece
                                                                        // when assignment operator is used with the
                                                                        // reference, it assigns the underlying value!
                                                                        // here BASE part is copy assigned ONLY 
                                                                        // by copy assignment (operator=)
                                                                        // object is sliced (derived part is lost) 

    i_ref.say();                                                        // thus still invokes a method from first
                                                                        // as derived part is still 'first'

    return 0;
}
