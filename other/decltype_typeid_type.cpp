#include <iostream>

class Clazz {};

template <typename T>
struct Result{
    using type = T;
};

template <typename T>
auto simple(T t) -> Result<T>;

int main() {
    typename Result<int>::type type;
    std::cout << typeid(type).name() << std::endl;

    decltype(simple(42))::type my_type;
    std::cout << typeid(my_type).name() << std::endl;

    return 0;
}
