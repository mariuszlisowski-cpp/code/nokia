#include <iostream>
#include <string>

class NumbersContainer {
public:
    using size_type = int;
    using value_type = int;
    
    NumbersContainer(size_type max_size) 
        : max_size(max_size)
        , count(0)
        , numbers(new value_type[max_size])
    {}

    NumbersContainer(NumbersContainer&& other) 
        : max_size(other.max_size)
        , count(other.count)
        , numbers(other.numbers)
    {
        other.numbers = nullptr;        
    }

    ~NumbersContainer() {
        delete [] numbers;
    }

    void add(value_type value) {
        numbers[count] = value;
        ++count;
    }

    void print_content() {
        for (size_type i = 0; i < count; ++i) {
            std::cout << "Position: " << i 
                      << " value: " << numbers[i] 
                      << std::endl ;
        }
    }

private:
    size_type max_size;
    size_type count;
    value_type* numbers;
};

NumbersContainer&& my_move(NumbersContainer& other) {
    return static_cast<NumbersContainer&&>(other);                          // alternative of std::move()
}

int main() {
    NumbersContainer containerA(10);

    containerA.add(1);
    containerA.add(2);
    containerA.add(3);

    containerA.print_content();

    NumbersContainer& container_ref(containerA);
    container_ref.print_content();

    NumbersContainer&& container_cos = static_cast<NumbersContainer&&>(containerA);
    container_cos.print_content();

    /*  */
    NumbersContainer containerB(static_cast<NumbersContainer&&>(container_cos));
    NumbersContainer containerC(my_move(container_cos));
    
    // containerA.print_content();                                          // inaccessible now
    
    return 0;
}
