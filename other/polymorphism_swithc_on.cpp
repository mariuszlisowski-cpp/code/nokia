#include <iostream>

struct A {
  virtual void foo() {                                      // virtual
    std::cout << "Hello from A" << std::endl;
  }
};

struct B : A {
  void foo() override {                                     // overrides from base (A)
    std::cout << "Hello from B" << std::endl;
  }
};

void call_foo(A* obj) {
  obj->foo();
}

int main() {
  B b;
  call_foo(&b);                                             // method accepting base struct pointer (struct A)

  return 0;
}
