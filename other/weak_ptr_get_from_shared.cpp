# include <memory>
# include <iostream>

template<class T>
struct Node {
    T value;
};

int main () {
    auto sp = std::shared_ptr<Node<int>>{new Node<int>{10}};
    auto wp = std::weak_ptr<Node<int>>(sp) ;
    if (not wp.expired()) {
        std::cout << wp.lock()->value << std::endl;                         // get pointer
    }
    sp.reset();
    if (not wp.expired()) {                                                 // weak not available
        std::cout << wp.lock()->value << std::endl;
    }
}
