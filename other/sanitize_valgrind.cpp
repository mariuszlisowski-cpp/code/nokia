// compile: g++ -g sanitize_valgrind.cpp -o sanitize_valgrind
// sanitize: valgrind --leak-check=yes ./sanitize_valgrind

int main() {
    /* not detected! */
    int* ptr = new int(9);
    // delete ptr;                                      // memory leak!

    /* not detected! */
    int* arr = new int[10];
    int el = arr[10];                                   // out of bound!

    return 0;
}
