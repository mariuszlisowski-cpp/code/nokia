#include <iostream>

struct A {
    virtual ~A() = default;                                     // dynamic_cast can only work on polymorphic types
                                                                // thus a destructor (or methods) must be virtual
    std::string name;                                       
};

class B : public A {};

class C {};

int main() {
    B b;
    b.name = "foo";
    
    A* aptr = &b;                                               // upcasting (SAFE)

    B* bptr = dynamic_cast<B*>(aptr);                           // dynamic dowcasting may return nullptr
    if (bptr) {                                                 // thus must be checked (SAFE)
        std::cout << bptr->name << std::endl;
    }

    if (!dynamic_cast<C*>(aptr)) {                              // SAFE check inside a condition
        std::cout << "not a C object" << std::endl;
    }

    return 0;
}
