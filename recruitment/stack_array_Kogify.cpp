/* task by Sebastian Szczecina */
#include <iostream>

constexpr int size{ 2 };

class Stack {
public:
    Stack() {}
    void push(int value) {
        if (counter < size) {
            stack[counter++] = value; 
        } else {
            std::cout << "max size reached" << std::endl; }
        }
    
    int pop() { 
        if (counter > 0) {
            // int temp = stack[counter - 1];
            // --counter;
            // return temp;
            return stack[--counter];
        }
        
        return -1;
    }
    
    int top() {
        if (counter > 0) {
            return stack[counter - 1];
        }
        
        return -1;
    }
    int get_counter() {
        return counter;
    }
    
private:
    int counter{};
    int stack[size];
};

int main() {
    Stack stack;

    std::cout << "top: " << stack.top() << std::endl;
    stack.push(42);
    std::cout << "top: " << stack.top() << std::endl;
    stack.push(43);
    std::cout << "top: " << stack.top() << std::endl;
    stack.push(44);
    std::cout << "top: " << stack.top() << std::endl;
 
    std::cout << "counter: " << stack.get_counter() << std::endl;

    auto popped = stack.pop();
    std::cout << "popped: " << popped << std::endl;
    std::cout << "top: " << stack.top() << std::endl;

    std::cout << "counter: " << stack.get_counter() << std::endl;
    
    popped = stack.pop();
    std::cout << "popped: " << popped << std::endl;
    popped = stack.pop();
    std::cout << "popped: " << popped << std::endl;
}
