#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <vector>

/* Based on input from entrance/exit gates which record hour of entry/exit to the object please create a function which will check at what hour number of users was maximal.
    - sum up both genders
    - please use algorithms from std lib
    Expected result for data above:
    Number of users at 6 o'clock: 2
    Number of users at 7 o'clock: 3
    Number of users at 8 o'clock: 7
    Number of users at 9 o'clock: 7
    Number of users at 10 o'clock: 4
    Number of users at 11 o'clock: 2
    Number of users at 12 o'clock: 0
 */

template<typename T>
std::vector<T>& operator+=(std::vector<T>& lhs,
                           const std::vector<T>& rhs)
{
    lhs.insert(lhs.end(), rhs.begin(), rhs.end());
    return lhs;
}

void gate_counter(std::vector<int>& in_women, const std::vector<int>& in_men,
             std::vector<int>& out_women, const std::vector<int>& out_men)
{
    /* concatenate comings & goings */
    in_women += in_men;
    out_women += out_men;

    /* count comings at each hour */
    std::map<int, int> comings;
    for (auto& hour : in_women) {
        ++comings[hour];
    }

    /* count goings at each hour */
    std::map<int, int> goings;
    for (auto& hour : out_women) {
        ++goings[hour];
    }

    /* print persons staying inside at each hour */
    auto first_hour = comings.begin()->first;
    auto last_hour = goings.rbegin()->first;
    for (auto [current_comings, current_goings, i] = std::tuple(0, 0, first_hour); i <= last_hour; ++i) {
        current_comings += comings[i];
        current_goings += goings[i];
        std::cout << "at " << i << ", " 
                  << current_comings - current_goings << " persons inside" << std::endl;
    }
}

int main() {
    std::vector<int> in_women = {6,6,8,8,9};
    std::vector<int> in_men = {7,8,8};

    std::vector<int> out_men = {10,11,11};
    std::vector<int> out_women = {9,10,10,12,12};

    gate_counter(in_women, in_men, out_men, out_women);

    return 0;
}
