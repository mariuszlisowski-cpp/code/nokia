/* g++ -std=c++17 -lgtest -lgtest_main type_inference.cpp
*/
#include <gtest/gtest.h>
#include <string>
#include <type_traits>
#include <utility>

struct V {};

template<typename T>
struct Result
{
    using type = T;
};

V Lv;                                                                             // lvalue
const V cLv;                                                                      // const lvalue
V& Lvr = Lv;                                                                      // lvalue reference
V Rv() { return V{}; }                                                            // rvalue
V&& Rvr = Rv();                                                                   // rvalue reference
const V& cLvr = cLv;                                                              // const lvalue reference
const V cRv() { return V{}; }                                                     // const rvalue
const V&& cRvr = cRv();                                                           // const rvalue reference

/* By-double-ampersand does not mean by rvalue-reference!
   It is a forwarding reference, carries information on argument value category:  // infered:
   ▶ infers Type  for arguments of rvalue type category                           // TYPE (lvalue)
   ▶ infers Type& for arguments of lvalue type category                           // REF  (lvalue reference)
   ▶ infers Type& for arguments of rvalue references!                             // REF* (lvalue reference)
     for forwarding need to bump rvalue references to rvalue value category by
     std::forward or a suitable cast.
   ▶ reference collapsing: 
     Type&     is         Type&     1 -> 1                                        // rule:
     Type&&    reduces to Type&     2 -> 1                                        // 1, 2, 3 -> 1 &
     Type& &&  reduces to Type&     3 -> 1                                        // 4 -> 2 &&
     Type&& &  reduces to Type&     3 -> 1                                      
     Type&& && reduces to Type&&    4 -> 2 */
template<typename T>                                                              // returns what T is (not t)
auto universal(T&& t) -> Result<T>;                                               // T is lvalue or lvalue reference
TEST(TypeInferenceTest, UniversalReferenceAsParameter)                            // inferring passed CONSTness
{
    static_assert(std::is_same_v<decltype(universal(Lv))::type, V&>);             // lvalue reference             REF
    static_assert(std::is_same_v<decltype(universal(cLv))::type, const V&>);      // const lvalue reference       REF
    static_assert(std::is_same_v<decltype(universal(Lvr))::type, V&>);            // lvalue reference             REF
    static_assert(std::is_same_v<decltype(universal(Rv()))::type, V>);            // lvalue                      TYPE
    static_assert(std::is_same_v<decltype(universal(Rvr))::type, V&>);            // lvalue reference             REF*
    static_assert(std::is_same_v<decltype(universal(cLvr))::type, const V&>);     // const lvalue reference       REF
    static_assert(std::is_same_v<decltype(universal(cRv()))::type, const V>);     // const lvalue                TYPE
    static_assert(std::is_same_v<decltype(universal(cRvr))::type, const V&>);     // const lvalue reference       REF*
}

/* By-reference-to-lvalue parameter will infer CONST from argument and bind to it
   (cannot be done for non-const rvalue). Can bind to const temporaries! */
template<typename T>
auto by_reference(T& t) -> Result<T>;                                             // T is lvalue
TEST(TypeInferenceTest, LvalueReferenceAsParameter)                               // inferring passed CONSTness
{
    static_assert(std::is_same_v<decltype(by_reference(Lv))::type, V>);           // lvalue
    static_assert(std::is_same_v<decltype(by_reference(cLv))::type, const V>);    // const lvalue
    static_assert(std::is_same_v<decltype(by_reference(Lvr))::type, V>);          // lvalue
    // static_assert(std::is_same_v<decltype(by_reference(Rv()))::type, V>);      // ERROR (cannot bind as non-const)
    static_assert(std::is_same_v<decltype(by_reference(Rvr))::type, V>);          // lvalue
    static_assert(std::is_same_v<decltype(by_reference(cLvr))::type, const V>);   // const lvalue
    static_assert(std::is_same_v<decltype(by_reference(cRv()))::type, const V>);  // const lvalue (can bind as const)
    static_assert(std::is_same_v<decltype(by_reference(cRvr))::type, const V>);   // const lvalue (can bind as const)
}
/* By-reference-to-const-lvalue parameter will bind to any argument. */
template<typename T>
auto by_const_reference(const T& t) -> Result<T>;                                 // t is const
TEST(TypeInferenceTest, ConstLvalueReferenceAsParameter)                          // T is always lvalue
{
    static_assert(std::is_same_v<decltype(by_const_reference(Lv))::type, V>);     // lvalue
    static_assert(std::is_same_v<decltype(by_const_reference(cLv))::type, V>);    // lvalue
    static_assert(std::is_same_v<decltype(by_const_reference(Lvr))::type, V>);    // lvalue
    static_assert(std::is_same_v<decltype(by_const_reference(Rv()))::type, V>);   // lvalue (can bind)
    static_assert(std::is_same_v<decltype(by_const_reference(Rvr))::type, V>);    // lvalue
    static_assert(std::is_same_v<decltype(by_const_reference(cLvr))::type, V>);   // lvalue
    static_assert(std::is_same_v<decltype(by_const_reference(cRv()))::type, V>);  // lvalue
    static_assert(std::is_same_v<decltype(by_const_reference(cRvr))::type, V>);   // lvalue
}

/* By-copy parameter will get a copy of the argument type
   (regardless of const) */
template<typename T>
auto by_value(T t) -> Result<T>;                                                  // returns what T is (not t)
TEST(TypeInferenceTest, CopyAsParameter)
{
    static_assert(std::is_same_v<decltype(by_value(Lv))::type, V>);               // lvalue
    static_assert(std::is_same_v<decltype(by_value(cLv))::type, V>);              // lvalue
    static_assert(std::is_same_v<decltype(by_value(Lvr))::type, V>);              // lvalue
    static_assert(std::is_same_v<decltype(by_value(Rv()))::type, V>);             // lvalue
    static_assert(std::is_same_v<decltype(by_value(Rvr))::type, V>);              // lvalue
    static_assert(std::is_same_v<decltype(by_value(cLvr))::type, V>);             // lvalue
    static_assert(std::is_same_v<decltype(by_value(cRv()))::type, V>);            // lvalue
    static_assert(std::is_same_v<decltype(by_value(cRvr))::type, V>);             // lvalue
}
template<typename T>
auto by_const_value(const T t) -> Result<T>;                                      // t is const
TEST(TypeInferenceTest, ConstCopyAsParameter)                                     // T is always lvalue
{
    static_assert(std::is_same_v<decltype(by_const_value(Lv))::type, V>);         // lvalue
    static_assert(std::is_same_v<decltype(by_const_value(cLv))::type, V>);        // lvalue
    static_assert(std::is_same_v<decltype(by_const_value(Lvr))::type, V>);        // lvalue
    static_assert(std::is_same_v<decltype(by_const_value(Rv()))::type, V>);       // lvalue
    static_assert(std::is_same_v<decltype(by_const_value(Rvr))::type, V>);        // lvalue
    static_assert(std::is_same_v<decltype(by_const_value(cLvr))::type, V>);       // lvalue
    static_assert(std::is_same_v<decltype(by_const_value(cRv()))::type, V>);      // lvalue
    static_assert(std::is_same_v<decltype(by_const_value(cRvr))::type, V>);       // lvalue
}
