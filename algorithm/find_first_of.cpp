/* finds any value from one container in another */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector<int> long_vector{ 7 , 3 , 1 , 2 , 3 , 6 , 1 , 2 , 3, 10 };
    std::vector<int> short_vector{ -1, -2, 3 };

    auto it = std::find_first_of(long_vector.begin(), long_vector.end(),            // is any in short vector
                                 short_vector.begin(), short_vector.end());         // in a long one?
    
    if (it != long_vector.end()) {
        std::cout << "one element of 'short' found in 'long' at index " 
                  << std::distance(long_vector.begin(), it) << std::endl;
        std::cout << "found element: " << *it << std::endl;
    }

    return 0;
}
