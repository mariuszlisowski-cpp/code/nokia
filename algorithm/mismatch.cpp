#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> v1{ 1 , 2 , 3 , 4 , 10 , 6 , 7 };
    std::vector<int> v2{ 1 , 2 , 3 , 4 , 5 , 6 , 7 };    

    auto [from_v1, from_v2] = std::mismatch(v1.begin(), v1.end(),
                                            v2.begin());                    // 10 !=5 at index 4
    
    if (from_v1 != v1.end()) {
        std::cout << "first:  mismatch at index " 
                  << std::distance(v1.begin(), from_v1)
                  << " with value " << *from_v1 << std::endl;
    }

    if (from_v2 != v2.end()) {
        std::cout << "second: mismatch at index " 
                  << std::distance(v2.begin(), from_v2)
                  << " with value " << *from_v2 << std::endl;
    }

    return 0;
}
