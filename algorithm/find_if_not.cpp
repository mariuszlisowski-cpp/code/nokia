#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>

template<int n>
auto is_greater_than = std::bind(std::greater<>(), std::placeholders::_1, n);

int main() {
    std::vector<int> v{ -9, - 7, 3 , 4 , 2 , 6 , 7 };

    auto it1 = std::find_if_not(v.begin(), v.end(), is_greater_than<-2>);         // first not greater than ...
    auto it2 = std::find_if(v.begin(), v.end(), is_greater_than<-2>);             // first greater than ...

    if (it1 != v.end()) {
        std::cout << *it1 << std::endl;
    }
    if (it2 != v.end()) {
        std::cout << *it2 << std::endl;
    }
    
    return 0;
}
