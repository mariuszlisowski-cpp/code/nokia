#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector<int> v1{ 1 , 2 , 3 , 4 , 5 };
    std::vector<int> v2{ 10 , 20 , 30 , 40 , 50 };

    std::transform(v1.begin(), v1.end(),
                   v2.begin(),
                   std::ostream_iterator<int>(std::cout, " "),
                   std::plus<>());
    
    return 0;
}
