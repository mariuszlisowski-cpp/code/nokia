/* Rearranges elements such that the range contains the sorted middle - 
   first smallest elements in the range [first, last).
   The order of equal elements is not guaranteed to be preserved.
   The order of the remaining elements in the range [middle, last) is unspecified. */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector<int> v{ 9, 8, 7, 6, 5, 4, 3, 2, 1 };
    /*                                mid
                        1  2  3  4  5 unspecified
    */
    auto middle = std::next(v.begin() + v.size() / 2);                      // five elements (0-4)
    std::cout << "distance: " << std::distance(v.begin(), middle);
    std::cout << "\nmiddle : " << *middle << std::endl;
    std::partial_sort(v.begin(),                                            // first
                      middle,                                               // sort up to middle (excluded)
                      v.end());                                             // last

    std::copy(v.begin(), v.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
    std::copy(v.begin(), middle,                                            // print sorted only
              std::ostream_iterator<int>(std::cout, " "));

    return 0;
}
