#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    /*  */
    auto min = std::min({ 1, 2, 3, 4, 5});                                          // value
    auto max = std::max({ 1, 2, 3, 4, 5});
    std::cout << "min: " << min << ", max: " << max << std::endl;

    std::tie(min, max) = std::minmax({ 1, 2, 3, 4, -5});                            // value
    std::cout << "min: " << min << ", max: " << max << std::endl;

    /*  */
    std::vector<int> v{ 1, 2, 3, 4, 5 };
    auto min_element = std::min_element(v.begin(), v.end());                        // iterator
    auto max_element = std::max_element(v.begin(), v.end());
    std::cout << "min el: " << *min_element 
              << ", max el: " << *max_element << std::endl;

    std::tie(min_element, max_element) = std::minmax_element(v.begin(), v.end());   // iterator
    std::cout << "min el: " << *min_element 
              << ", max el: " << *max_element << std::endl;

    return 0;
}
