/* Reorders the elements in the range in such a way that all elements for which 
   the predicate returns TRUE precede the elements for which predicate returns FALSE. 
   Relative order of the elements IS preserved. */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

using Iter = std::vector<int>::const_iterator;

void print(Iter it1, Iter it2) {
    std::copy(it1, it2,
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    /* relative order preserved */
    std::vector<int> v{ 1, 5, 6, 1, 3, 6, 1, 8, 4, 9, 5, 3 };
    //                        6        6     8     9                        // left half (bigger than)
    //                  1  5     1  3     1     4     5  3                  // right half (smaller or equal)

    std::cout << "size: " <<  v.size() << std::endl;

    auto partition_point =                                                  // iterator to range mid point
        std::stable_partition(v.begin(), v.end(),
                       [](const auto& el) {
                           return el > 5;                                   // to the left if TRUE
                       });                                                  // PRESERVE RELATIVE ORDER
    
    print(v.begin(), partition_point);                                      // left half
    print(partition_point, v.end());                                        // right half

    /* partition point check */
    partition_point =
        std::partition_point(v.begin(), v.end(),
                        [](const auto& el) {
                            return el > 5;                                   // check if correctly partitioned
                        });
    std::cout << "index: " << std::distance(v.begin(), partition_point) 
              << ", with value: " << *partition_point << std::endl;
                         
    return 0;
}
