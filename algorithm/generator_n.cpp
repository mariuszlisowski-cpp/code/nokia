#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector<char> v(10);                                                // n elements
                                                                            // initialized with zero

    auto generator = [ch{ 'a' }]() mutable { return ch++; };
    std::generate_n(v.begin(), v.size() / 2, generator);

    std::copy(v.begin(), v.end(),
              std::ostream_iterator<int>(std::cout, " | "));
    std::cout << std::endl;
    std::copy(v.begin(), v.end(),
              std::ostream_iterator<char>(std::cout, " | "));               // half non-printable
    

    return 0;
}
