/* actually copies elements from the first range to another range,
   omitting the elements which satisfy specific criteria */
   
#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

int main() {
    std::string str{ "one, two, three, four," };
    
    auto past_copied = 
        std::remove_copy(str.begin(), str.end(), 
                           std::ostream_iterator<char>(std::cout),          // print...
                           ',');

    // v.erase(past_copied, v.end());                                       // no need to erase

    std::cout << '\n' <<  str << std::endl;

    return 0;
}
