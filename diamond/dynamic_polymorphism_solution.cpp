#include <cstddef>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>

/* 1. problem: diamond problem
      ITransmiter     Diagnoser
          \           /       \
           Transmitter        Receiver      <- Diagnoser c'tor called twice
                      \       /
                        Radio

   2. solution: virtual inheritance
      ITransmiter     Diagnoser
          \           /       \
           Transmitter        Receiver      <- virtual inheritance
                      \       /
                        Radio               <- Diagnoser c'tor called once */

/* interface */
struct ITransmitter {
    virtual void send(const std::string& text) = 0;
    virtual ~ITransmitter() = default;
};

/* abstract class */
struct Diagnoser {
    Diagnoser(const std::string& file_name) : file_name(file_name) {}
    virtual ~Diagnoser() = default;

    virtual void diagnose(const std::string& message) {
        const auto line = message + '\n';
        std::ofstream out(file_name, std::ofstream::app);                   // append
        out.write(line.c_str(), line.length());
        out.close();
    }

private:
    std::string file_name;
};

// struct Transmitter : ITransmitter, protected Diagnoser {                 // diamond problem (!)
struct Transmitter : ITransmitter, virtual protected Diagnoser {            // virtual inheritance
    Transmitter() : Diagnoser("transmitter.txt") {                          // filename may be needed
        std::cout << "Tranmitter created" << std::endl;
    }

    // virtual ~Transmitter() {                                             // redundant virtual as ...
    ~Transmitter() {                                                        // interface d'ctor is already virtual
        std::cout << "Tranmitter destoryed" << std::endl;
    }
    // virtual void send(const std::string& text) {                         // redundant virtual as ...
    void send(const std::string& text) override {                           // interface method is already virtual
        std::cout << "Transmitter sending: " << text << std::endl;
        diagnose("sent: " + text);
    }
};

// struct Receiver : protected Diagnoser {                                  // diamond problem (!)
struct Receiver : virtual protected Diagnoser {                             // virtual inheritance
    Receiver() : Diagnoser("receiver.txt") {                                // filename may be needed
        std::cout << "Reciver created" << std::endl;
    }

    std::string receive() {
        diagnose("text received");
        return "Text received";
    }
    // virtual ~Receiver() {                                                // redundant virtual as ...
    ~Receiver() {                                                           // abstract class d'ctor is already virtual
        std::cout << "Receiver destoryed" << std::endl;
    }
};

struct Radio : Transmitter, Receiver {                                      // diamond problem solution ...
    Radio() : Diagnoser("radio.txt") {}                                     // Diagnoser c'tor called
    void send(const std::string& text) override {
        diagnose("radio is sending: " + text);                              // clear now (!)
        // Transmitter::diagnose("radio is sending: " + text);              // no need to call specific method
        constexpr int num_of_chars_to_send = 10;
        for (size_t i = 0; i < text.size(); i += 10) {
            if (i + 10 > text.size()) {
                auto s = text.substr(i, text.length() - i);
                Transmitter::send(s);                                       // no recursion (!)
            } else {
                Transmitter::send(text.substr(i, num_of_chars_to_send));
            }
        }
    }
};

int main() {
    auto radio = std::make_unique<Radio>();
    radio->send("ala ma kota, ala ma kota, ala ma kota");
    radio->receive();

    /* create transmitter diagnose file */
    Transmitter t;
    t.send("text written to transmitter.txt file");                         // transmitter c'tor
                                                                            // Diagnoser file created
    return 0;
}
