#include <cstddef>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>

/* 1. problem: diamond problem
      ITransmiter     Diagnoser
          \           /       \
           Transmitter        Receiver      <- Diagnoser c'tor called twice
                      \       /
                        Radio

   2. solution: virtual inheritance
      ITransmiter     Diagnoser
          \           /       \
           Transmitter        Receiver      <- virtual inheritance
                      \       /
                        Radio               <- Diagnoser c'tor called once */

/* interface */
struct ITransmitter {
    virtual void send(const std::string& text) = 0;
    virtual ~ITransmitter() = default;
};

/* abstract class */
struct Diagnoser {
    Diagnoser(const std::string& file_name) : file_name(file_name) {}
    virtual ~Diagnoser() = default;

    virtual void diagnose(const std::string& message) {
        const auto line = message + '\n';
        std::ofstream out(file_name, std::ofstream::app);                   // append
        out.write(line.c_str(), line.length());
        out.close();
    }

private:
    std::string file_name;
};

struct Transmitter : ITransmitter, protected Diagnoser {                    // diamond problem (!)
//                                 virtual protected Diagnoser              // should inherit virtually
    Transmitter() : Diagnoser("transmitter.txt") {
        std::cout << "Tranmitter created" << std::endl;
    }

    // virtual ~Transmitter() {                                             // redundant virtual as ...
    ~Transmitter() {                                                        // interface d'ctor is already virtual
        std::cout << "Tranmitter destoryed" << std::endl;
    }
    // virtual void send(const std::string& text) {                         // redundant virtual as ...
    void send(const std::string& text) override {                           // interface method is already virtual
        std::cout << "Transmitter sending: " << text << std::endl;
        diagnose("sent: " + text);
    }
};

struct Receiver : protected Diagnoser {                                     // diamond problem (!)
//                virtual protected Diagnoser                               // should inherit virtually
    Receiver() : Diagnoser("receiver.txt") {
        std::cout << "Reciver created" << std::endl;
    }

    std::string receive() {
        diagnose("text received");
        return "Text received";
    }
    // virtual ~Receiver() {
    ~Receiver() {                                                           // virtual redundant
        std::cout << "Receiver destoryed" << std::endl;
    }
};

struct Radio : Transmitter, Receiver {
//  Radio() : Diagnoser("radio.txt") {}                                     // Diagnoser c'tor should be called
    void send(const std::string& text) override {
        // diagnose("radio is sending: " + text);                           // diamond problem: ambiguity (!)
        Transmitter::diagnose("radio is sending: " + text);                 // clear now but problem exists
        constexpr int num_of_chars_to_send = 10;
        for (size_t i = 0; i < text.size(); i += 10) {
            if (i + 10 > text.size()) {
                auto s = text.substr(i, text.length() - i);
                Transmitter::send(s);                                       // no recursion (!)
            } else {
                Transmitter::send(text.substr(i, num_of_chars_to_send));    // thus transmitter called
            }
        }
    }
};

int main() {
    auto radio = std::make_unique<Radio>();
    radio->send("ala ma kota, ala ma kota, ala ma kota");
    radio->receive();

    return 0;
}
