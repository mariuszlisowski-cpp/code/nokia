/* unique pointer implementation 
   single header with separate template implementations */
#pragma once

#include <algorithm>
#include <stdexcept>

template <typename T>
class unique_ptr {
public:
    unique_ptr();                                                   // default c'tor
    unique_ptr(T* ptr);                                             // parametrized c'tor
    ~unique_ptr();                                                  // d'tor
    
    /* copy semantics */
    unique_ptr(const unique_ptr&) = delete;                         // copy c'tor
    unique_ptr& operator=(unique_ptr& otherPtr) = delete;           // copy assignment

    /* move semantics */
    unique_ptr(unique_ptr&& otherPtr);                              // move c'tor
    unique_ptr& operator=(unique_ptr&& otherPtr);                   // move assignment
    
    /* operator overloading */
    T& operator*() const;
    T* operator->() const;

    /* helper methods */
    T* get() const;
    T* release();
    void reset(T* newPtr = nullptr);
    

private:
    T* ptr_;
};

template <typename T>
unique_ptr<T>::unique_ptr() : ptr_(nullptr) {}

template <typename T>
unique_ptr<T>::unique_ptr(T* ptr) : ptr_(ptr) {}

template <typename T>
unique_ptr<T>::unique_ptr(unique_ptr&& otherPtr)
    : ptr_(otherPtr.release()) {}

template <typename T>
unique_ptr<T>::~unique_ptr() {
    delete ptr_;
}

template <typename T>
T* unique_ptr<T>::get() const {
    return ptr_;
}

template <typename T>
T* unique_ptr<T>::release() {
    T* temp = nullptr;
    std::swap(temp, ptr_);
    return temp;
}

template <typename T>
void unique_ptr<T>::reset(T* newPtr) {
    delete ptr_;
    ptr_ = newPtr;
}

template <typename T>
T& unique_ptr<T>::operator*() const {
    if (!ptr_) {
        std::runtime_error("nullptr");
    };
    return *ptr_;
}

template <typename T>
T* unique_ptr<T>::operator->() const {
    return ptr_;
}

template <typename T>
unique_ptr<T>& unique_ptr<T>::operator=(unique_ptr<T>&& otherPtr) {
    if (this != &otherPtr) {
        delete ptr_;
        ptr_ = otherPtr.ptr_;
        otherPtr.ptr_ = nullptr;
    }
    return *this;
}
