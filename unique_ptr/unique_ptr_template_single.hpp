/* unique pointer implementation 
   single header with inline implementations */
#pragma once

#include <stdexcept>

template <typename T>
class unique_ptr {
public:
    unique_ptr() : ptr_(nullptr) {}                                 // default c'tor
    unique_ptr(T* ptr) : ptr_(ptr) {}                               // parametrized c'tor
    ~unique_ptr() {                                                 // d'tor
        delete ptr_;
    }
    
    /* copy semantics */
    unique_ptr(const unique_ptr&) = delete;                         // copy c'tor
    unique_ptr& operator=(unique_ptr& other) = delete;              // copy assignment
    unique_ptr& operator=(nullptr_t) noexcept {                     // permitted (!)
        reset(); 
        return *this;
    }

    /* move semantics */
    unique_ptr(unique_ptr&& other) : ptr_(other.release()) {}       // move c'tor
    unique_ptr& operator=(unique_ptr&& other) {                     // move assignment
        if (this != &other) {
            delete ptr_;
            ptr_ = other.ptr_;
            other.ptr_ = nullptr;
        }
        return *this;
    }

    /* operator overloading */
    T& operator*() const {
        if (!ptr_) {
            std::runtime_error("nullptr");
        };
        return *ptr_;
    }

    T* operator->() const {
        return ptr_;
    }

    /* helper methods */
    T* get() const {
        return ptr_;
    }
    T* release() {                                                  // swaps with nullptr
        T* temp = ptr_;
        ptr_ = nullptr;
        return temp;
    }
    void reset(T* ptr = nullptr) {
        delete ptr_;
        ptr_ = ptr;
    }

private:
    T* ptr_;
};
