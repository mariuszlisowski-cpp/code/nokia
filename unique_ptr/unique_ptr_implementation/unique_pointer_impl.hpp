/* unique pointer implementation */
#pragma once

template<typename T>
class unique_ptr {
public:
    unique_ptr() : ptr_(nullptr) {}                             // default c'tor
    unique_ptr(T* ptr) : ptr_(ptr) {}                           // parametrized c'tor
    ~unique_ptr() {                                             // d'tor
        delete ptr_;
    }

    /* copy semantics */
    unique_ptr(const unique_ptr&) = delete;                     // copy c'tor
    unique_ptr& operator=(const unique_ptr&) = delete;          // copy assignment
    
    /* move semantics */
    unique_ptr(unique_ptr<T>&& other) noexcept                  // move c'tor
        : ptr_(other.ptr_)                                      // ptr_(other.release()) {}
    {
            other.ptr_ = nullptr;
    }
    unique_ptr& operator=(unique_ptr&& other) noexcept {
        if (this != &other) {                                   // self assignment check
            delete ptr_;
            ptr_ = other.ptr_;
            other.ptr_ = nullptr;
        }
        return *this;
    }

    /* operator overloading */
    T& operator*() const {                                      // may throw except
        return *ptr_;                                           // if nullptr
    }
    T* operator->() const {
        return ptr_;
    }

    /* helper methods */
    T* get() const {
        return ptr_;
    }
    T* release() {                                              // swaps with nullptr
        T* temp = ptr_;
        ptr_ = nullptr;
        return temp;
    }
    void reset(T* ptr = nullptr) {
        delete ptr_;
        ptr_ = ptr;
    }

private:
    T* ptr_{};
};
