#include "unique_pointer_impl.hpp"

#include <iostream>

struct Empty {
    void no_action() {}
};

int main() {
    unique_ptr ptrA{new int{ 5 }};

    std::cout << *ptrA << std::endl;
    std::cout << *ptrA.get() << std::endl;
    
    auto raw = ptrA.release();
    delete raw;

    unique_ptr ptrB = std::move(ptrA);

    ptrA.reset(new int{ 9 });
    std::cout << *ptrA << std::endl;
    ptrA.reset();

    unique_ptr ptrC(new Empty());
    ptrC->no_action();

    return 0;
}
