#include <iostream>
#include <memory>

int main() {
    int a = 42;
    int* raw_ptrA = &a;                                             // raw pointer on STACK
    // auto s_ptrA =  std::shared_ptr<int>(raw_ptrA);               // ERROR: raw pointer on STACK!

    int* raw_ptrB = new int(42);                                    // raw pointer on HEAP
    auto s_ptrB =  std::shared_ptr<int>(raw_ptrB);                  // OK: copy made from raw pointer
    auto s_ptrC =  std::shared_ptr<int>(s_ptrB);                    // OK: copy made from shared pointer
    auto s_ptrD = s_ptrC;                                           // OK: another copy

    auto s_ptr_quite_new = std::shared_ptr<int>(new int(24));       // new object and its pointer

    std::cout << s_ptrB.use_count() << std::endl;                   // three pointing to the same (with 42)

    return 0;
}
