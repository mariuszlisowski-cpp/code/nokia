#include <cassert>
#include <iostream>
#include <memory>

class Foo {
public:
    Foo(int value) : value_(value) {}

    std::shared_ptr<Foo> clone() {
        // return std::shared_ptr<Foo>(this);                       // ERROR: double ownership
        // return std::shared_ptr<Foo>(new Foo(*this));             // OK
        return std::make_shared<Foo>(*this);                        // OK
    }

    bool operator==(const Foo& other) const {
        return true;
    }

    int value_;
};

int main() {
    std::shared_ptr<Foo> foo_ptrA = std::make_shared<Foo>(42);
    std::shared_ptr<Foo> foo_ptrB = foo_ptrA->clone();              // cloned (independent copy created)

    std::cout << foo_ptrA << std::endl;                             // points to one location
    std::cout << foo_ptrB << std::endl;                             // points to OTHER location

    std::cout << foo_ptrA->value_ << std::endl;
    std::cout << foo_ptrB->value_ << std::endl;

    std::cout << foo_ptrA.use_count() << std::endl;
    std::cout << foo_ptrB.use_count() << std::endl;

    foo_ptrB->value_ = 24;
    std::cout << foo_ptrA->value_ << std::endl;
    std::cout << foo_ptrB->value_ << std::endl;
    
    assert(*foo_ptrA == *foo_ptrB);                                 // OK, passes (why? operator ==)
    assert(foo_ptrA == foo_ptrB);                                   // OK, addresses of objects DIFFERENT
                                                                    // thus FAILS
    return 0;
}
